import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_getx_concept/models/buyer.match.info.dart';
import 'package:flutter_getx_concept/models/seller.match.info.dart';
import 'package:get/get.dart';

class SellsModel {
  SellsModel({
    String id,
    String amountGet = "",
    String phone = "",
    String dayWait = "",
    String status = "",
    String timeCreated,
    BuyerMatchInfoModel buyerMatchInfo,
  }) {
    this.id = id;
    this.amountGet = amountGet;
    this.phone = phone;
    this.dayWait = dayWait;
    this.status = status;
    this.timeCreated = timeCreated;
    this.buyerMatchInfo = buyerMatchInfo;
  }

  RxString _timeCreated = RxString();
  set timeCreated(String value) => _timeCreated.value = value;
  String get timeCreated => _timeCreated.value;

  RxString _status = RxString();
  set status(String value) => _status.value = value;
  String get status => _status.value;

  RxString _id = RxString();
  set id(String value) => _id.value = value;
  String get id => _id.value;

  RxString _amountGet = RxString();
  set amountGet(String value) => _amountGet.value = value;
  String get amountGet => _amountGet.value;

  RxString _phone = RxString();
  set phone(String value) => _phone.value = value;
  String get phone => _phone.value;

  RxString _dayWait = RxString();
  set dayWait(String value) => _dayWait.value = value;
  String get dayWait => _dayWait.value;

  Rx<BuyerMatchInfoModel> _buyerMatchInfo = Rx<BuyerMatchInfoModel>();
  set buyerMatchInfo(BuyerMatchInfoModel value) => _buyerMatchInfo.value = value;
  BuyerMatchInfoModel get buyerMatchInfo => _buyerMatchInfo.value;
}
