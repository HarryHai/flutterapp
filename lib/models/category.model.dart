import 'package:get/get.dart';

class CategoryModel {
  CategoryModel({
    String key,
    String name,
    String image,
    int index,
  }) {
    this.key = key;
    this.image = image;
    this.name = name;
    this.index = index;
  }

  RxString _key = RxString();
  set key(String value) => _key.value = value;
  String get key => _key.value;

  RxString _name = RxString();
  set name(String value) => _name.value = value;
  String get name => _name.value;

  RxString _image = RxString();
  set image(String value) => _image.value = value;
  String get image => _image.value;

  RxInt _index = RxInt();
  set index(int value) => _index.value = value;
  int get index => _index.value;
}
