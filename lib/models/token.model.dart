import 'package:get/get.dart';

class TokenModel {
  TokenModel({
    String id,
    String amount = "",
    String toPhone = "",
    String fromPhone = "",
    String timeCreated,
  }) {
    this.id = id;
    this.amount = amount;
    this.toPhone = toPhone;
    this.fromPhone = fromPhone;
    this.timeCreated = timeCreated;
  }

  RxString _timeCreated = RxString();
  set timeCreated(String value) => _timeCreated.value = value;
  String get timeCreated => _timeCreated.value;

  RxString _id = RxString();
  set id(String value) => _id.value = value;
  String get id => _id.value;

  RxString _amount = RxString();
  set amount(String value) => _amount.value = value;
  String get amount => _amount.value;

  RxString _toPhone = RxString();
  set toPhone(String value) => _toPhone.value = value;
  String get toPhone => _toPhone.value;

  RxString _fromPhone = RxString();
  set fromPhone(String value) => _fromPhone.value = value;
  String get fromPhone => _fromPhone.value;
}
