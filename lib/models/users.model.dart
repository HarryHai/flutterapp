import 'package:get/get.dart';

class UsersModel {
  UsersModel({
    String id,
    String name ="",
    String phone ="",
    String cmndNumber ="",
    String address ="",
    String image ="",
    String birth ="",
    String email ="",
  }) {
    this.id = id;
    this.name = name;
    this.phone = phone;
    this.cmndNumber = cmndNumber;
    this.address = address;
    this.image = image;
    this.birth = birth;
    this.email = email;
  }

  RxString _id = RxString();
  set id(String value) => _id.value = value;
  String get id => _id.value;

  RxString _name = RxString();
  set name(String value) => _name.value = value;
  String get name => _name.value;

  RxString _phone = RxString();
  set phone(String value) => _phone.value = value;
  String get phone => _phone.value;

  RxString _cmndNumber = RxString();
  set cmndNumber(String value) => _cmndNumber.value = value;
  String get cmndNumber => _cmndNumber.value;

  RxString _image = RxString();
  set image(String value) => _image.value = value;
  String get image => _image.value;

  RxString _address = RxString();
  set address(String value) => _address.value = value;
  String get address => _address.value;

  RxString _email = RxString();
  set email(String value) => _email.value = value;
  String get email => _email.value;

  RxString _birth = RxString();
  set birth(String value) => _birth.value = value;
  String get birth => _birth.value;
}
