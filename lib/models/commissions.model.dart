import 'package:get/get.dart';

class CommisModel {
  CommisModel({
    String id,
    String amount = "",
    String phone = "",
    String fromPhone = "",
    String timeCreated,
  }) {
    this.id = id;
    this.amount = amount;
    this.phone = phone;
    this.fromPhone = fromPhone;
    this.timeCreated = timeCreated;
  }

  RxString _timeCreated = RxString();
  set timeCreated(String value) => _timeCreated.value = value;
  String get timeCreated => _timeCreated.value;

  RxString _id = RxString();
  set id(String value) => _id.value = value;
  String get id => _id.value;

  RxString _amount = RxString();
  set amount(String value) => _amount.value = value;
  String get amount => _amount.value;

  RxString _phone = RxString();
  set phone(String value) => _phone.value = value;
  String get phone => _phone.value;

  RxString _fromPhone = RxString();
  set fromPhone(String value) => _fromPhone.value = value;
  String get fromPhone => _fromPhone.value;
}
