import 'package:flutter_getx_concept/models/seller.match.info.dart';
import 'package:get/get.dart';

class BuysModel {
  BuysModel({
    String id,
    String phone = "",
    String status = "",
    String uid = "",
    String timeCreated,
    SellerMatchInfoModel sellerMatchInfo,
  }) {
    this.id = id;
    this.phone = phone;
    this.status = status;
    this.uid = uid;
    this.timeCreated = timeCreated;
    this.sellerMatchInfo = sellerMatchInfo;
  }
  RxString _timeCreated = RxString();
  set timeCreated(String value) => _timeCreated.value = value;
  String get timeCreated => _timeCreated.value;

  RxString _id = RxString();
  set id(String value) => _id.value = value;
  String get id => _id.value;

  RxString _phone = RxString();
  set phone(String value) => _phone.value = value;
  String get phone => _phone.value;

  RxString _status = RxString();
  set status(String value) => _status.value = value;
  String get status => _status.value;

  RxString _uid = RxString();
  set uid(String value) => _uid.value = value;
  String get uid => _uid.value;

  Rx<SellerMatchInfoModel> _sellerMatchInfo = Rx<SellerMatchInfoModel>();
  set sellerMatchInfo(SellerMatchInfoModel value) => _sellerMatchInfo.value = value;
  SellerMatchInfoModel get sellerMatchInfo => _sellerMatchInfo.value;
}
