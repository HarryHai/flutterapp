import 'package:get/get.dart';

class TransactionModel {
  TransactionModel({
    String id,
    String amount = "",
    String otherPhone = "",
    String type = "",
    String timeCreated,
  }) {
    this.id = id;
    this.amount = amount;
    this.otherPhone = otherPhone;
    this.type = type;
    this.timeCreated = timeCreated;
  }

  RxString _timeCreated = RxString();
  set timeCreated(String value) => _timeCreated.value = value;
  String get timeCreated => _timeCreated.value;

  RxString _id = RxString();
  set id(String value) => _id.value = value;
  String get id => _id.value;

  RxString _amount = RxString();
  set amount(String value) => _amount.value = value;
  String get amount => _amount.value;

  RxString _otherPhone = RxString();
  set otherPhone(String value) => _otherPhone.value = value;
  String get otherPhone => _otherPhone.value;

  RxString _type = RxString();
  set type(String value) => _type.value = value;
  String get type => _type.value;
}
