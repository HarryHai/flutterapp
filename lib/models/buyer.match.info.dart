import 'package:get/get.dart';

class BuyerMatchInfoModel {
  BuyerMatchInfoModel({
    String name = "",
    String phone,
    String confirmPicUrl,
  }) {
    this.name = name;
    this.confirmPicUrl = confirmPicUrl;
    this.phone = phone;
  }

  RxString _name = RxString();
  set name(String value) => _name.value = value;
  String get name => _name.value;

  RxString _phone = RxString();
  set phone(String value) => _phone.value = value;
  String get phone => _phone.value;

  RxString _confirmPicUrl = RxString();
  set confirmPicUrl(String value) => _confirmPicUrl.value = value;
  String get confirmPicUrl => _confirmPicUrl.value;
}
