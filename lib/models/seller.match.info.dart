import 'package:get/get.dart';

class SellerMatchInfoModel {
  SellerMatchInfoModel({
    int amountGet,
    String nameGet = "",
    String phoneGet,
  }) {
    this.amountGet = amountGet;
    this.nameGet = nameGet;
    this.phoneGet = phoneGet;
  }

  RxString _nameGet = RxString();
  set nameGet(String value) => _nameGet.value = value;
  String get nameGet => _nameGet.value;

  RxInt _amountGet = RxInt();
  set amountGet(int value) => _amountGet.value = value;
  int get amountGet => _amountGet.value;

  RxString _phoneGet = RxString();
  set phoneGet(String value) => _phoneGet.value = value;
  String get phoneGet => _phoneGet.value;
}
