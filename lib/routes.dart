import 'package:flutter_getx_concept/pages/buys/index.dart';
import 'package:flutter_getx_concept/pages/buys_detail/index.dart';
import 'package:flutter_getx_concept/pages/commissions/index.dart';
import 'package:flutter_getx_concept/pages/sells/index.dart';
import 'package:flutter_getx_concept/pages/sells_detail/index.dart';
import 'package:flutter_getx_concept/pages/token/index.dart';
import 'package:flutter_getx_concept/pages/token_detail/index.dart';
import 'package:flutter_getx_concept/pages/transaction/index.dart';
import 'package:flutter_getx_concept/pages/transaction_detail/index.dart';
import 'package:get/route_manager.dart';

import 'pages/cart/index.dart';
import 'pages/commission_detail/index.dart';
import 'pages/home/index.dart';
import 'pages/users/index.dart';
import 'pages/users_detail/index.dart';

routes() => [
  GetPage(name: "/", page: () => Home()),

  GetPage(name: "/users", page: () => Users()),
  GetPage(name: "users/:id", page: () => UserDetail()),

  GetPage(name: "/buys", page: () => Buys()),
  GetPage(name: "/buys/:id", page: () => BuyDetail()),

  GetPage(name: "/sells", page: () => Sells()),
  GetPage(name: "/sells/:id", page: () => SellsDetail()),

  GetPage(name: "/commissions", page: () => Commissions()),
  GetPage(name: "/commissions/:id", page: () => CommissionDetail()),

  GetPage(name: "/transaction", page: () => Transaction()),
  GetPage(name: "/transaction/:id", page: () => TransactionDetail()),

  GetPage(name: "/token", page: () => Tokens()),
  GetPage(name: "/token/:id", page: () => TokenDetail()),

  GetPage(name: "/cart", page: () => Cart()),
];