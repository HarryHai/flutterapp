import 'package:flutter_getx_concept/models/transaction.model.dart';
import 'package:flutter_getx_concept/pages/transaction/controller.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';

class TransfersDetailController extends GetxController {
  TransactionController controller = Get.find();
  RxList<TransactionModel> rxList = RxList<TransactionModel>([]);
  Rx<TransactionModel> _transfers = Rx<TransactionModel>();

  setTransfer(TransactionModel value) => _transfers.value = value;

  TransactionModel get transfers {
    return _transfers.value;
  }

  TransfersDetailController() {
    loadTransfersDetail();
  }

  loadTransfersDetail() async {
    try {
      rxList.addAll(controller.getListTransfers());
      rxList.forEach((user) {
        if (user.id == Get.parameters["id"].toString()) {
          setTransfer(user);
        }
      });
    } catch (error) {
      print(error.toString());
    }
  }
}
