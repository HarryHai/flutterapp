import 'package:flutter/material.dart';
import 'package:flutter_getx_concept/models/token.model.dart';
import 'package:flutter_getx_concept/models/transaction.model.dart';
import 'package:flutter_getx_concept/utils/colors.dart';
import 'package:get/get.dart';

class TokenItem extends StatelessWidget {
  final TokenModel tokenModel;

  TokenItem(this.tokenModel);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Get.toNamed("/token/${tokenModel.id.toString()}");
      },
      child: Container(
        width: double.infinity,
        child: Stack(
          overflow: Overflow.visible,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                top: 15,
                left: 15,
                right: 15,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "FromPhone: " + this.tokenModel.fromPhone ?? "",
                          style: TextStyle(
                            fontSize: 14,
                            color: AppColors.DARK,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(height: 5),
                        Text(
                          "ToPhone: " + this.tokenModel.toPhone ?? "",
                          style: TextStyle(
                            fontSize: 14,
                            color: AppColors.DARK,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(height: 5),
                        Text(
                          "Amount: " + this.tokenModel.amount ?? "",
                          style: TextStyle(
                            fontSize: 14,
                            color: AppColors.DARK,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(height: 5),
                        Text(
                          "TimeCreated: " + this.tokenModel.timeCreated,
                          style: TextStyle(
                            fontSize: 14,
                            color: AppColors.LIGHT_GREEN,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Divider(color: Colors.blueGrey),
          ],
        ),
      ),
    );
  }
}
