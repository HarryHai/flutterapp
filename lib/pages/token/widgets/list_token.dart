import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controller.dart';
import 'item_token.dart';

class TokenList extends StatelessWidget {
  final TokenController controller = Get.find();
  final double itemHeight = 130;
  final double itemWidth = Get.width / 2 - 100;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        return SliverList(
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return TokenItem(controller.tokenList.elementAt(index));
            },
            childCount: controller.tokenList.length ?? 0,
          ),
        );
      },
    );
  }
}
