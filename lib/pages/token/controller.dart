import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_getx_concept/models/token.model.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';

class TokenController extends GetxController {
  RxList<TokenModel> tokenList = RxList<TokenModel>([]);
  RxList<TokenModel> tokenListTemp = RxList<TokenModel>([]);

  // Collection reference
  final CollectionReference tokenCollection =
      FirebaseFirestore.instance.collection('getTokens');

  TokenController() {
    loadTransfers();
  }

  loadTransfers() async {
    List<TokenModel> tokenList = await getTokenList();
    this.tokenList.value = tokenList;
    this.tokenListTemp.value = tokenList;
  }

  Future<List<TokenModel>> getTokenList() async {
    List<TokenModel> listData = [];
    QuerySnapshot querySnapshot = await tokenCollection.get();
    for (int i = 0; i < querySnapshot.docs.length; i++) {
      var token = querySnapshot.docs[i].data();
      var time = (token.containsKey("timeCreated") ? token["timeCreated"].toDate().toString() : "");
      var tokenModel = (TokenModel(
        id: querySnapshot.docs[i].id,
        amount: (token.containsKey("amount") ? token['amount'].toString() : ""),
        toPhone: (token.containsKey("toPhone") ? token['toPhone'] : ""),
        fromPhone: (token.containsKey("fromPhone") ? token['fromPhone'] : ""),
        timeCreated: time,
      ));
        listData.add(tokenModel);
        print("hainm11=${listData.length}");
    }
    return listData;
  }
  void searchToken(String key) {
    List<TokenModel> dataSearch = tokenListTemp
        .where((item) =>
    item.toPhone.toString().trim().toLowerCase().contains(key.trim().toString())||
        item.fromPhone.toString().trim().toLowerCase().contains(key.trim().toString()))
        .map<TokenModel>((transfers) => transfers)
        .toList();
    tokenList.value = dataSearch;
  }
  RxList<TokenModel> getListToken() {
    return tokenList;
  }
}
