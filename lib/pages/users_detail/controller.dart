import 'package:flutter_getx_concept/models/users.model.dart';
import 'package:flutter_getx_concept/pages/users/controller.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';

class UserDetailController extends GetxController {
  UserController userController = Get.find();
  Rx<UsersModel> _user = Rx<UsersModel>();

  setUser(UsersModel value) => _user.value = value;

  UsersModel get user {
    return _user.value;
  }

  UserDetailController() {
    loadUser();
  }

  loadUser() async {
    try {
      // List<dynamic> data = await loadJson("assets/data/users.json");
      // dynamic item = data.firstWhere((item) {
      //   return item["id"].toString() == Get.parameters["id"].toString();
      // });
      RxList<UsersModel> rxList = userController.getListUser();
      rxList.forEach((user) {
        if (user.id == Get.parameters["id"].toString()){
          setUser(user);
        }
      });
      // setUser(UsersModel(
      //   id: item["id"],
      //   name: item["name"],
      //   category: item["category"],
      //   price: item["price"],
      //   image: item["image"],
      //   description: item["description"],
      // ));
    } catch (error) {
      print(error.toString());
    }
  }

// adduser() {
//   try {
//     CartItemModel cartItem =
//         appController.cartItems.value.firstWhere((cartItem) {
//       return cartItem.user.id == this.user.id;
//     });
//     cartItem.incrementQuantity();
//   } catch (error) {
//     appController.cartItems.add(CartItemModel(
//       user: this.user,
//       quantity: 1,
//     ));
//   }
//   Get.back();
// }
}
