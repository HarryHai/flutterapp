import 'package:flutter/material.dart';
import 'package:flutter_getx_concept/models/users.model.dart';
import 'package:flutter_getx_concept/utils/colors.dart';
import 'package:get/get.dart';

import '../controller.dart';

class UserDetails extends StatelessWidget {
  final UserDetailController controller = Get.find();
  final UsersModel usersModel;

  UserDetails(this.usersModel);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(25),
      decoration: BoxDecoration(
        color: AppColors.WHITE,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25),
          topRight: Radius.circular(25),
        ),        
      ),
      child: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(bottom: 25),
              margin: EdgeInsets.only(bottom: 25),
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    color: AppColors.LIGHT,
                    width: 1,
                    style: BorderStyle.solid,
                  ),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Obx(
                    () => Center(
                      child: Text(
                        usersModel?.name ?? "",
                        style: TextStyle(
                          fontSize: 32,
                          color: AppColors.DARK,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 15),
                  Obx(
                        () =>
                        Center(
                          child: Text(
                            usersModel?.address ?? "",
                            style: TextStyle(
                              fontSize: 14,
                              color: AppColors.DARK,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                        ),
                  ),
                ],
              ),
            ),
            Row(
              children: <Widget>[
                Text(
                  "Phone: ",
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.DARK,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  usersModel?.phone ?? "",
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.LIGHT_GREEN,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Text(
                  "Cmnd: ",
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.DARK,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  usersModel?.cmndNumber ?? "",
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.LIGHT_GREEN,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Text(
                  "Email: ",
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.DARK,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  usersModel?.email ?? "",
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.LIGHT_GREEN,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ), Row(
              children: <Widget>[
                Text(
                  "Birth: ",
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.DARK,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  usersModel?.birth ?? "",
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.LIGHT_GREEN,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
