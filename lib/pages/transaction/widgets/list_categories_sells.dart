import 'package:flutter/material.dart';
import 'package:flutter_getx_concept/models/category.model.dart';
import 'package:flutter_getx_concept/pages/sells/controller.dart';
import 'package:flutter_getx_concept/pages/sells/widgets/item_categories_sells.dart';
import 'package:get/get.dart';

class ListCategoriesSell extends StatelessWidget {
  final SellsController controller = Get.find();

  ListCategoriesSell();

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return SizedBox(
        height: 100,
        child: ListView.separated(
          scrollDirection: Axis.horizontal,
          padding: EdgeInsets.symmetric(horizontal: 25, vertical: 25),
          separatorBuilder: (BuildContext context, int index) =>
              SizedBox(width: 50),
          itemCount: controller.categories.length,
          itemBuilder: (_, index) {
            CategoryModel category = controller.categories.elementAt(index);
            return Obx(() {
              return ItemCategoriesSells(
                category,
                category == controller.selectedCategory,
              );
            });
          },
        ),
      );
    });
  }
}
