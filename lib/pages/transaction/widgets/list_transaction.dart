import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controller.dart';
import 'item_transfers.dart';

class TransactionList extends StatelessWidget {
  final TransactionController controller = Get.find();
  final double itemHeight = 130;
  final double itemWidth = Get.width / 2 - 100;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        return SliverList(
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return TransactionItem(controller.transactionList.elementAt(index));
            },
            childCount: controller.transactionList.length ?? 0,
          ),
        );
      },
    );
  }
}
