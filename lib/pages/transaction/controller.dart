import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_getx_concept/models/transaction.model.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';

class TransactionController extends GetxController {
  RxList<TransactionModel> transactionList = RxList<TransactionModel>([]);

  RxList<TransactionModel> transactionListTemp = RxList<TransactionModel>([]);

  // Collection reference
  final CollectionReference commisCollection =
      FirebaseFirestore.instance.collection('transaction');

  TransactionController() {
    loadTransaction();
  }

  loadTransaction() async {
    List<TransactionModel> transfersList = await getTransfers();
    this.transactionList.value = transfersList;
    this.transactionListTemp.value = transfersList;
  }

  Future<List<TransactionModel>> getTransfers() async {
    List<TransactionModel> listData = [];
    QuerySnapshot querySnapshot = await commisCollection.get();
    for (int i = 0; i < querySnapshot.docs.length; i++) {
      var transfer = querySnapshot.docs[i].data();
      var time = (transfer.containsKey("timeCreated") ? transfer["timeCreated"]
          .toDate()
          .toString() : "");
      var transfersModel = (TransactionModel(
        id: querySnapshot.docs[i].id,
        amount: (transfer.containsKey("amount")
            ? transfer['amount'].toString()
            : ""),
        type: (transfer.containsKey("type") ? transfer['type'] : ""),
        otherPhone:
        (transfer.containsKey("otherPhone") ? transfer['otherPhone'] : ""),
        timeCreated: time,));
      listData.add(transfersModel);
    }
    return listData;
  }

  void searchTransfers(String key) {
    List<TransactionModel> dataSearch = transactionListTemp
        .where((item) =>
        item.otherPhone.toString().trim().contains(key.trim().toString()))
        .map<TransactionModel>((transfers) => transfers)
        .toList();
    transactionList.value = dataSearch;
  }

  RxList<TransactionModel> getListTransfers() {
    return transactionList;
  }
}
