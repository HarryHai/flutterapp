import 'package:flutter/material.dart';
import 'package:flutter_getx_concept/pages/buys/controller.dart';
import 'package:flutter_getx_concept/pages/buys/widgets/item_buys.dart';
import 'package:get/get.dart';

class BuysList extends StatelessWidget {
  final BuysController controller = Get.find();
  final double itemHeight = 130;
  final double itemWidth = Get.width / 2 - 100;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        return SliverList(
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return BuysItem(controller.buys.elementAt(index),index);
            },
            childCount: controller.buys.length ?? 0,
          ),
        );
      },
    );
  }
}
