import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_getx_concept/models/buys.model.dart';
import 'package:flutter_getx_concept/utils/colors.dart';
import 'package:flutter_getx_concept/widgets/product_image.dart';
import 'package:get/get.dart';

class BuysItem extends StatelessWidget {
  final BuysModel buy;
  final int index;

  BuysItem(this.buy, this.index);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Get.toNamed("/buys/${buy.id.toString()}");
      },
      child: Container(
        width: double.infinity,
        child: Stack(
          overflow: Overflow.visible,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                top: 15,
                left: 10,
                right: 10,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  CachedNetworkImage(
                    imageUrl: this.buy.sellerMatchInfo.nameGet,
                    imageBuilder: (context, imageProvider) => Container(
                      width: 40.0,
                      height: 40.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: imageProvider, fit: BoxFit.cover),
                      ),
                    ),
                    placeholder: (context, url) => CircularProgressIndicator(),
                    errorWidget: (context, url, error) => ProductImage(
                      "assets/images/ic_buys.png",
                      width: 40,
                      height: 40,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Name: " + this.buy.sellerMatchInfo.nameGet ?? "",
                          style: TextStyle(
                            fontSize: 14,
                            color: AppColors.DARK,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Text(
                          "Phone: " + this.buy.phone ?? "",
                          style: TextStyle(
                            fontSize: 14,
                            color: AppColors.DARK,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(height: 5),
                        Text(
                          "Status: " + this.buy.status ?? "",
                          style: TextStyle(
                            fontSize: 14,
                            color: AppColors.DARK,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(height: 5),
                        Text(
                          "TimeCreated: " + this.buy.timeCreated,
                          style: TextStyle(
                            fontSize: 14,
                            color: AppColors.LIGHT_GREEN,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Divider(color: index != 0 ? Colors.blueGrey : AppColors.LIGHT),
          ],
        ),
      ),
    );
  }
}
