import 'package:flutter/material.dart';
import 'package:flutter_getx_concept/models/category.model.dart';
import 'package:flutter_getx_concept/pages/buys/controller.dart';
import 'package:flutter_getx_concept/pages/buys/widgets/item_categories_buys.dart';
import 'package:get/get.dart';

class ListCategoriesBuys extends StatelessWidget {
  final BuysController controller = Get.find();

  ListCategoriesBuys();

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Padding(
        padding: const EdgeInsets.only(left: 4.0, right: 4.0),
        child: SizedBox(
          height: 40,
          child: GridView.builder(
            itemCount: controller.categories.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              crossAxisSpacing: 4.0,
            ),
            itemBuilder: (BuildContext context, int index) {
              CategoryModel category = controller.categories.elementAt(index);
              return Obx(() {
                return ItemCategoriesBuys(
                  category,
                  category == controller.selectedCategory.value,
                );
              });
            },
          ),
        ),
      );
    });
  }
}
