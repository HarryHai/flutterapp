import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_getx_concept/models/buys.model.dart';
import 'package:flutter_getx_concept/models/category.model.dart';
import 'package:flutter_getx_concept/models/seller.match.info.dart';
import 'package:flutter_getx_concept/utils/json.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';

class BuysController extends GetxController {
  RxList<BuysModel> buys = RxList<BuysModel>([]);
  RxList<BuysModel> buysTemp = RxList<BuysModel>([]);

  RxList<CategoryModel> categories = RxList<CategoryModel>([]);
  Rx<CategoryModel> selectedCategory = Rx<CategoryModel>();
  List<BuysModel> buysData = [];

  // Collection reference
  final CollectionReference buysCollection =
      FirebaseFirestore.instance.collection('buys');

  BuysController() {
    loadBuys();
  }

  loadBuys() async {
    List<dynamic> dataCategories = await loadJson(
      "assets/data/categories_buys.json",
    );
    categories.addAll(dataCategories
        .map<CategoryModel>((category) => CategoryModel(
              key: category["key"],
              name: category["name"],
              index: category["index"],
            ))
        .toList());
    await getBuysData();
    selectCategory(categories.first);
  }

  selectCategory(CategoryModel categoryModel) async {
    selectedCategory.value = categoryModel;
    buys.value = buysData
        .where((item) => item.status == selectedCategory.value.key)
        .map<BuysModel>((buy) => buy)
        .toList();
    buysTemp.value = buysData
        .where((item) => item.status == selectedCategory.value.key)
        .map<BuysModel>((buy) => buy)
        .toList();
  }

  Future getBuysData() async {
    QuerySnapshot querySnapshot = await buysCollection.get();
    for (int i = 0; i < querySnapshot.docs.length; i++) {
      var buy = querySnapshot.docs[i].data();
      var sellerMatchInfo;
      if (buy.containsKey("sellerMatchInfo") != null) {
        Map<dynamic, dynamic> data = buy['sellerMatchInfo'];
        sellerMatchInfo = (SellerMatchInfoModel(
          amountGet: (data.containsKey("amountGet") && data["amountGet"] != null
              ? data["amountGet"]
              : 0),
          phoneGet: (data.containsKey("phoneGet") && data["phoneGet"] != null
              ? data["phoneGet"]
              : ""),
          nameGet: (data.containsKey("nameGet") && data["nameGet"] != null
              ? data["nameGet"]
              : ""),
        ));
      }
      var time = (buy.containsKey("timeCreated") && buy["timeCreated"] != null
          ? buy["timeCreated"].toDate().toString()
          : "");
      var buysModel = (BuysModel(
          id: querySnapshot.docs[i].id,
          phone: (buy.containsKey("phone") && buy["phone"] != null
              ? buy["phone"]
              : ""),
          status: (buy.containsKey("status") && buy["status"] != null
              ? buy["status"]
              : ""),
          uid: (buy.containsKey("uid") && buy["uid"] != null ? buy["uid"] : ""),
          timeCreated: time,
          sellerMatchInfo: sellerMatchInfo ??
              SellerMatchInfoModel(amountGet: 0, phoneGet: "")));
      buysData.add(buysModel);
      print("hainm11=${buysData.length}");
    }
  }

  void searchBuys(String key) {
    List<BuysModel> dataSearch = buysTemp
        .where((item) =>
        item.phone.toString().trim().contains(key.trim().toString()))
        .map<BuysModel>((user) => user)
        .toList();
    buys.value = dataSearch;
  }

  RxList<BuysModel> getListBuys() {
    return buys;
  }
}
