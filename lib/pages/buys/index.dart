import 'package:flutter/material.dart';
import 'package:flutter_getx_concept/pages/buys/controller.dart';
import 'package:flutter_getx_concept/pages/buys/widgets/list_buys.dart';
import 'package:flutter_getx_concept/pages/buys/widgets/list_categories_buys.dart';
import 'package:flutter_getx_concept/utils/colors.dart';
import 'package:flutter_getx_concept/widgets/appbar_action.dart';
import 'package:flutter_getx_concept/widgets/custom_appbar.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';

class Buys extends StatelessWidget {
  final TextEditingController textEditingController =
      new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<BuysController>(
      init: BuysController(),
      builder: (controller) {
        return Scaffold(
          backgroundColor: AppColors.LIGHT,
          appBar: CustomAppBar(
            "Buys",
            leadings: [
              CustomAppBarAction(
                    () => Get.back(),
                Feather.arrow_left,
              ),
            ],
          ),
          body: CustomScrollView(
            slivers: <Widget>[SliverPadding(
              padding: const EdgeInsets.only(bottom: 8.0),
              sliver: SliverAppBar(
                backgroundColor: AppColors.LIGHT,
                elevation: 0.0,
                automaticallyImplyLeading: false,
                pinned: false,
                floating: false,
                title: TextField(
                  onChanged: (value) => {controller.searchBuys(value)},
                  controller: textEditingController,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 19.0),
                    fillColor: AppColors.LIGHT,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(
                            50.0))),
                    suffixIcon: IconButton(
                      // onPressed: () {
                      //   controller.searchUser(textEditingController.text);
                      // },
                      color: AppColors.GREEN,
                      icon: Icon(Icons.search),
                    ),
                    hintText: 'Search ...',
                  ),
                ),
              ),
            ),
              SliverToBoxAdapter(
                child: ListCategoriesBuys(),
              ),
              BuysList()
            ],
          ),
        );
      },
    );
  }
}
