import 'package:flutter_getx_concept/models/category.model.dart';
import 'package:flutter_getx_concept/utils/json.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';

class HomeController extends GetxController {
  RxList<CategoryModel> categories = RxList<CategoryModel>([]);
  HomeController() {
    loadCategories();
  }

  loadCategories() async {
    //Load categories
    List<dynamic> dataCategories = await loadJson(
      "assets/data/categories_home.json",
    );
    categories.addAll(dataCategories
        .map<CategoryModel>((category) => CategoryModel(
              key: category["key"],
              name: category["name"],
              index: category["index"],
              image: category["image"],
            ))
        .toList());
  }
}
