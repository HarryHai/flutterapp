import 'package:flutter/material.dart';
import 'package:flutter_getx_concept/models/category.model.dart';
import 'package:flutter_getx_concept/utils/colors.dart';
import 'package:flutter_getx_concept/widgets/product_image.dart';
import 'package:get/get.dart';

class ReportItem extends StatelessWidget {
  final CategoryModel categoryModel;

  ReportItem(this.categoryModel);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Get.toNamed("/${this.categoryModel.key}");
      },
      child: Container(
        decoration: BoxDecoration(
          color: AppColors.WHITE,
          boxShadow: <BoxShadow>[
            BoxShadow(
              blurRadius: 15,
              color: AppColors.SHADOW,
              offset: Offset(6, 10),
            ),
          ],
          borderRadius: BorderRadius.all(
            Radius.circular(8),
          ),
        ),
        child: Stack(
          overflow: Overflow.visible,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                top: 25,
                bottom: 15,
                left: 25,
                right: 15,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Center(
                    child: ProductImage(
                      this.categoryModel.image,
                      width: 50,
                      height: 50,
                    ),
                  ),
                  Center(
                    child: Text(
                      this.categoryModel.name,
                      style: TextStyle(
                        fontSize: 16,
                        color: AppColors.DARK,
                        fontWeight: FontWeight.bold,
                      ),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
