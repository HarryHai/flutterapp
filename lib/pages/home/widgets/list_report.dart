import 'package:flutter/material.dart';
import 'package:flutter_getx_concept/pages/home/controller.dart';
import 'package:get/get.dart';

import 'item_home_report.dart';

class HomeReportGrid extends StatelessWidget {
  final HomeController controller = Get.find();
  final double itemHeight = 100;
  final double itemWidth = Get.width / 2 - 100;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        return SliverGrid(
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 25.0,
              crossAxisSpacing: 25.0,
              childAspectRatio: 1),
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return ReportItem(controller.categories.elementAt(index));
            },
            childCount: controller.categories.length ?? 0,
          ),
        );
      },
    );
  }
}
