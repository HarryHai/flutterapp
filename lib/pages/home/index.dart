import 'package:flutter/material.dart';
import 'package:flutter_getx_concept/pages/home/controller.dart';
import 'package:flutter_getx_concept/utils/colors.dart';
import 'package:flutter_getx_concept/widgets/custom_appbar.dart';
import 'package:get/get.dart';

import 'widgets/list_report.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      init: HomeController(),
      builder: (controller) {
        return Scaffold(
          backgroundColor: AppColors.LIGHT,
          appBar: CustomAppBar(
            "Dashboard",
            actions: [],
          ),
          body: Padding(
            padding: const EdgeInsets.only(left: 25.0, right: 25.0),
            child: CustomScrollView(
              slivers: <Widget>[
                HomeReportGrid(),
                new SliverPadding(
                  padding: new EdgeInsets.all(5.0),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
