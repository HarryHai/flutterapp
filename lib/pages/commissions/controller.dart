import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_getx_concept/models/commissions.model.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';

class CommissionsController extends GetxController {
  RxList<CommisModel> commisList = RxList<CommisModel>([]);
  RxList<CommisModel> commisListTemp = RxList<CommisModel>([]);

  // Collection reference
  final CollectionReference commisCollection =
      FirebaseFirestore.instance.collection('commissions');

  CommissionsController() {
    loadUsers();
  }

  loadUsers() async {
    List<CommisModel> commisList = await getUsers();
    this.commisList.value = commisList;
    this.commisListTemp.value = commisList;
  }

  Future<List<CommisModel>> getUsers() async {
    List<CommisModel> listData = [];
    QuerySnapshot querySnapshot = await commisCollection.get();
    for (int i = 0; i < querySnapshot.docs.length; i++) {
      var commission = querySnapshot.docs[i].data();
        var time = (commission.containsKey("timeCreated") ? commission["timeCreated"].toDate().toString() : "");
        var usersModel = (CommisModel(
          id: querySnapshot.docs[i].id,
          amount: (commission.containsKey("amount") ? commission['amount'].toString() : ""),
          phone: (commission.containsKey("phone") ? commission['phone'] : ""),
          fromPhone: (commission.containsKey("fromPhone") ? commission['fromPhone'] : ""),
          timeCreated: time,
        ));
        listData.add(usersModel);
        print("hainm11=${listData.length}");
    }
    return listData;
  }
  void searchCommissions(String key) {
    List<CommisModel> dataSearch = commisListTemp
        .where((item) =>
        item.phone.toString().trim().contains(key.trim().toString())||
        item.fromPhone.toString().trim().contains(key.trim().toString()))
        .map<CommisModel>((transfers) => transfers)
        .toList();
    commisList.value = dataSearch;
  }
  RxList<CommisModel> getListCommisstion() {
    return commisList;
  }
}
