import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controller.dart';
import 'item_commission.dart';

class CommissionsList extends StatelessWidget {
  final CommissionsController controller = Get.find();
  final double itemHeight = 130;
  final double itemWidth = Get.width / 2 - 100;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        return SliverList(
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return CommissionsItem(controller.commisList.elementAt(index));
            },
            childCount: controller.commisList.length ?? 0,
          ),
        );
      },
    );
  }
}
