import 'package:flutter_getx_concept/models/commissions.model.dart';
import 'package:flutter_getx_concept/pages/commissions/controller.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';

class CommissionsDetailController extends GetxController {
  CommissionsController controller = Get.find();
  RxList<CommisModel> rxList = RxList<CommisModel>([]);
  Rx<CommisModel> _commission = Rx<CommisModel>();

  setCommissions(CommisModel value) => _commission.value = value;

  CommisModel get commission {
    return _commission.value;
  }

  CommissionsDetailController() {
    loadCommissionDetail();
  }

  loadCommissionDetail() async {
    try {
      rxList.addAll(controller.getListCommisstion());
      rxList.forEach((user) {
        if (user.id == Get.parameters["id"].toString()) {
          setCommissions(user);
        }
      });
    } catch (error) {
      print(error.toString());
    }
  }
}
