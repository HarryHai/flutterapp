import 'package:flutter/material.dart';
import 'package:flutter_getx_concept/models/token.model.dart';
import 'package:flutter_getx_concept/pages/token/controller.dart';
import 'package:flutter_getx_concept/utils/colors.dart';
import 'package:get/get.dart';

class TokenDetailsInfo extends StatelessWidget {
  final TokenController controller = Get.find();
  final TokenModel tokenModel;

  TokenDetailsInfo(this.tokenModel);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(25),
      decoration: BoxDecoration(
        color: AppColors.WHITE,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25),
          topRight: Radius.circular(25),
        ),
      ),
      child: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(bottom: 25),
              margin: EdgeInsets.only(bottom: 25),
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    color: AppColors.LIGHT,
                    width: 1,
                    style: BorderStyle.solid,
                  ),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Obx(
                    () => Center(
                      child: Text(
                        "ToPhone: " + tokenModel.toPhone ?? "",
                        style: TextStyle(
                          fontSize: 14,
                          color: AppColors.DARK,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Row(
              children: <Widget>[
                Text(
                  "Amount: ",
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.DARK,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  tokenModel.amount.toString() ?? "",
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.LIGHT_GREEN,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Text(
                  "FromPhone: ",
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.DARK,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  tokenModel.fromPhone ?? "",
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.LIGHT_GREEN,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Text(
                  "TimeCreated: ",
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.DARK,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  tokenModel.timeCreated ?? "",
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.LIGHT_GREEN,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
