import 'package:flutter_getx_concept/models/token.model.dart';
import 'package:flutter_getx_concept/pages/token/controller.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';

class TokenDetailController extends GetxController {
  TokenController controller = Get.find();
  RxList<TokenModel> rxList = RxList<TokenModel>([]);
  Rx<TokenModel> _token = Rx<TokenModel>();

  setToken(TokenModel value) => _token.value = value;

  TokenModel get token {
    return _token.value;
  }

  TokenDetailController() {
    loadTokenDetail();
  }

  loadTokenDetail() async {
    try {
      rxList.addAll(controller.getListToken());
      rxList.forEach((user) {
        if (user.id == Get.parameters["id"].toString()) {
          setToken(user);
        }
      });
    } catch (error) {
      print(error.toString());
    }
  }
}
