import 'package:flutter_getx_concept/models/buys.model.dart';
import 'package:flutter_getx_concept/pages/buys/controller.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';

class BuysDetailController extends GetxController {
  BuysController controller = Get.find();
  RxList<BuysModel> rxList = RxList<BuysModel>([]);
  Rx<BuysModel> _buys = Rx<BuysModel>();

  setBuys(BuysModel value) => _buys.value = value;

  BuysModel get buy {
    return _buys.value;
  }

  BuysDetailController() {
    loadBuysDetail();
  }

  loadBuysDetail() async {
    try {
      rxList.addAll(controller.getListBuys());
      rxList.forEach((user) {
        if (user.id == Get.parameters["id"].toString()) {
          setBuys(user);
        }
      });
    } catch (error) {
      print(error.toString());
    }
  }
}
