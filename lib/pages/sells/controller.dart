import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_getx_concept/models/buyer.match.info.dart';
import 'package:flutter_getx_concept/models/category.model.dart';
import 'package:flutter_getx_concept/models/sells.model.dart';
import 'package:flutter_getx_concept/utils/json.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';

class SellsController extends GetxController {
  RxList<SellsModel> sells = RxList<SellsModel>([]);
  RxList<SellsModel> sellsTemp = RxList<SellsModel>([]);

  RxList<CategoryModel> categories = RxList<CategoryModel>([]);
  Rx<CategoryModel> selectedCategory = Rx<CategoryModel>();
  List<SellsModel> sellsData = [];

  // Collection reference
  final CollectionReference sellsCollection =
      FirebaseFirestore.instance.collection('sells');

  SellsController() {
    loadSells();
  }

  loadSells() async {
    List<dynamic> dataCategories = await loadJson(
      "assets/data/categories_sells.json",
    );
    categories.addAll(dataCategories
        .map<CategoryModel>((category) =>
        CategoryModel(
          key: category["key"],
          name: category["name"],
          index: category["index"],
        ))
        .toList());
    await getSellsData();
    selectCategory(categories.first);
  }

  selectCategory(CategoryModel category) async {
    selectedCategory.value = category;
    sells.value = sellsData
        .where((item) => item.status == category.key)
        .map<SellsModel>((sell) => sell)
        .toList();
    sellsTemp.value = sellsData
        .where((item) => item.status == category.key)
        .map<SellsModel>((sell) => sell)
        .toList();
  }

  Future getSellsData() async {
    QuerySnapshot querySnapshot = await sellsCollection.get();
    for (int i = 0; i < querySnapshot.docs.length; i++) {
      var sell = querySnapshot.docs[i].data();
      var buyerMatchInfo;
      if (sell.containsKey("buyerMatchInfo") != null) {
        Map<dynamic, dynamic> data = sell['buyerMatchInfo'];
        buyerMatchInfo = (BuyerMatchInfoModel(
          name: (data.containsKey("name") && data["name"] != null
              ? data["name"]
              : ""),
          phone: (data.containsKey("phone") && data["phone"] != null
              ? data["phone"]
              : ""),
          confirmPicUrl:
          (data.containsKey("confirmPicUrl") && data["confirmPicUrl"] != null
              ? data["confirmPicUrl"]
              : ""),
        ));
      }
      var time = (sell.containsKey("timeCreated")
          ? sell["timeCreated"].toDate().toString()
          : "");

      var sellsModel = (SellsModel(
          id: querySnapshot.docs[i].id,
          phone: (sell.containsKey("phone") && sell["phone"] != null
              ? sell["phone"]
              : ""),
          status: (sell.containsKey("status") && sell["status"] != null
              ? sell["status"]
              : ""),
          amountGet: (sell.containsKey("amountGet") && sell["amountGet"] != null
              ? sell["amountGet"].toString()
              : ""),
          dayWait:
          (sell.containsKey("dayWait") && sell["dayWait"] != null
              ? sell["dayWait"].toString()
              : ""),
          timeCreated: time,
          buyerMatchInfo: buyerMatchInfo ??
              BuyerMatchInfoModel(name: "", phone: "", confirmPicUrl: "")));
      sellsData.add(sellsModel);
      print("hainm11=${sellsData.length}");
    }
  }

  void searchSells(String key) {
    List<SellsModel> dataSearch = sellsTemp
        .where((item) =>
        item.phone.toString().trim().contains(key.trim().toString()))
        .map<SellsModel>((user) => user)
        .toList();
    sells.value = dataSearch;
  }

  RxList<SellsModel> getListSells() {
    return sells;
  }
}
