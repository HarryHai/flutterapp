import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_getx_concept/models/sells.model.dart';
import 'package:flutter_getx_concept/utils/colors.dart';
import 'package:flutter_getx_concept/widgets/product_image.dart';
import 'package:get/get.dart';

class SellsItem extends StatelessWidget {
  final SellsModel sell;
  final int index;

  SellsItem(this.sell, this.index);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Get.toNamed("/sells/${sell.id.toString()}");
      },
      child: Container(
        width: double.infinity,
        child: Stack(
          overflow: Overflow.visible,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                top: 15,
                left: 15,
                right: 15,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  CachedNetworkImage(
                    imageUrl: this.sell.buyerMatchInfo.confirmPicUrl,
                    imageBuilder: (context, imageProvider) => Container(
                      width: 40.0,
                      height: 40.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: imageProvider, fit: BoxFit.cover),
                      ),
                    ),
                    placeholder: (context, url) => CircularProgressIndicator(),
                    errorWidget: (context, url, error) => ProductImage(
                      "assets/images/ic_sells.png",
                      width: 40,
                      height: 40,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          "Name: " + this.sell.buyerMatchInfo.name ?? "",
                          style: TextStyle(
                            fontSize: 16,
                            color: AppColors.DARK,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Text(
                          "Phone: " + this.sell.phone ?? "",
                          style: TextStyle(
                            fontSize: 14,
                            color: AppColors.DARK,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(height: 5),
                        Text(
                          "Status: " + this.sell.status ?? "",
                          style: TextStyle(
                            fontSize: 14,
                            color: AppColors.DARK,
                            fontWeight: FontWeight.bold,
                          ),
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(height: 5),
                        Text(
                          "Date:" + this.sell.timeCreated,
                          style: TextStyle(
                            fontSize: 14,
                            color: AppColors.LIGHT_GREEN,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Divider(color: index != 0 ? Colors.blueGrey : AppColors.LIGHT),
          ],
        ),
      ),
    );
  }
}
