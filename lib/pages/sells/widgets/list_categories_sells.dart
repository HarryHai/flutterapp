import 'package:flutter/material.dart';
import 'package:flutter_getx_concept/models/category.model.dart';
import 'package:flutter_getx_concept/pages/sells/controller.dart';
import 'package:flutter_getx_concept/pages/sells/widgets/item_categories_sells.dart';
import 'package:get/get.dart';

class ListCategoriesSell extends StatelessWidget {
  final SellsController controller = Get.find();

  ListCategoriesSell();

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return Padding(
        padding: const EdgeInsets.only(left: 4.0, right: 4.0),
        child: SizedBox(
          height: 40,
          child: GridView.builder(
            itemCount: controller.categories.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              crossAxisSpacing: 4.0,
            ),
            itemBuilder: (BuildContext context, int index) {
              CategoryModel category = controller.categories.elementAt(index);
              return Obx(() {
                return ItemCategoriesSells(
                  category,
                  category == controller.selectedCategory.value,
                );
              });
            },
          ),
        ),
      );
    });
  }
}
