import 'package:flutter/material.dart';
import 'package:flutter_getx_concept/pages/sells/controller.dart';
import 'package:flutter_getx_concept/pages/sells/widgets/item_sells.dart';
import 'package:get/get.dart';

class SellsList extends StatelessWidget {
  final SellsController controller = Get.find();
  final double itemHeight = 130;
  final double itemWidth = Get.width / 2 - 100;

  @override
  Widget build(BuildContext context) {
    return Obx(
      () {
        return SliverList(
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return SellsItem(controller.sells.elementAt(index),index);
            },
            childCount: controller.sells.length ?? 0,
          ),
        );
      },
    );
  }
}
