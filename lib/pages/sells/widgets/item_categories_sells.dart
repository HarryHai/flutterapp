import 'package:flutter/material.dart';
import 'package:flutter_getx_concept/models/category.model.dart';
import 'package:flutter_getx_concept/pages/sells/controller.dart';
import 'package:flutter_getx_concept/utils/colors.dart';
import 'package:get/get.dart';

class ItemCategoriesSells extends StatelessWidget {
  final SellsController controller = Get.find();
  final CategoryModel category;
  final bool selected;

  ItemCategoriesSells(this.category, this.selected);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => controller.selectCategory(category),
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                  color: selected ? AppColors.WHITE : AppColors.GREEN,
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(3),
                    topLeft: Radius.circular(3),
                  ),
                  border: Border.all(
                      width: 2, color: AppColors.SHADOW, style: BorderStyle.none)),
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.only(top: 6.0, bottom: 6.0),
                  child: Text(
                    this.category.name,
                    style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.bold,
                      color: selected ? AppColors.GREEN : AppColors.WHITE,
                    ),
                  ),
                ),
              ),
            ),
            Visibility(
              visible: selected,
              child: Container(
                height: 2,
                decoration: BoxDecoration(
                  color: AppColors.GREEN,
                  // borderRadius: BorderRadius.circular(3),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
