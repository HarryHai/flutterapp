import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_getx_concept/models/users.model.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';

class UserController extends GetxController {
  RxList<UsersModel> users = RxList<UsersModel>([]);
  RxList<UsersModel> usersTemp = RxList<UsersModel>([]);

  final CollectionReference userCollection =
      FirebaseFirestore.instance.collection('users');

  UserController() {
    loadUsers();
  }

  loadUsers() async {
    List<UsersModel> dataUsers = await getUsers();
    users.value = dataUsers;
    usersTemp.value = dataUsers;
  }

  Future<List<UsersModel>> getUsers() async {
    List<UsersModel> listData = [];
    QuerySnapshot querySnapshot = await userCollection.get();
    for (int i = 0; i < querySnapshot.docs.length; i++) {
      var user = querySnapshot.docs[i].data();
        var usersModel = (UsersModel(
          id: querySnapshot.docs[i].id,
          name: (user.containsKey("name") ? user['name'] : ""),
          email: (user.containsKey("email") ? user['email'] : ""),
          phone: (user.containsKey("phone") ? user['phone'] : ""),
          cmndNumber: (user.containsKey("cmndNumber") ? user['cmndNumber'] : ""),
          address: (user.containsKey("address") ? user['address'] : ""),
          image: (user.containsKey("frontIdPicURL") ? user['frontIdPicURL'] : ""),
          birth: (user.containsKey("birth") ? user['birth'] : ""),
        ));
        listData.add(usersModel);
        print("hainm11=${listData.length}");
    }
    return listData;
  }

  // get user data
  Future getUserData(String email) async {
    userCollection
        .where('phone', isEqualTo: "+84363633307")
        .snapshots()
        .listen((data) => print('phone ${data.documents[0]['phone']}'));
  }

  RxList<UsersModel> getListUser() {
    return users;
  }

  void searchUser(String key) {
    List<UsersModel> dataSearch = usersTemp
        .where((item) =>
    item.name.toString().trim().toLowerCase().contains(
        key.toString().trim().toLowerCase()) ||
        item.phone.toString().trim().contains(key.trim().toString()))
        .map<UsersModel>((user) => user)
        .toList();
    users.value = dataSearch;
  }
}
