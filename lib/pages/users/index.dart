import 'package:flutter/material.dart';
import 'package:flutter_getx_concept/pages/users/controller.dart';
import 'package:flutter_getx_concept/utils/colors.dart';
import 'package:flutter_getx_concept/widgets/appbar_action.dart';
import 'package:flutter_getx_concept/widgets/custom_appbar.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';

import 'widgets/list_users.dart';

class Users extends StatelessWidget {
  final TextEditingController textEditingController =
  new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<UserController>(
      init: UserController(),
      builder: (controller) {
        return Scaffold(
          backgroundColor: AppColors.LIGHT,
          appBar: CustomAppBar("Users", leadings: [
            CustomAppBarAction(
                  () => Get.back(),
              Feather.arrow_left,
            ),
          ]),
          body: CustomScrollView(
            slivers: <Widget>[
              SliverAppBar(
                backgroundColor: AppColors.LIGHT,
                elevation: 0.0,
                automaticallyImplyLeading: false,
                pinned: false,
                floating: false,
                title: TextField(
                  onChanged: (value) => {controller.searchUser(value)},
                  controller: textEditingController,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.only(left: 19.0),
                    fillColor: AppColors.LIGHT,
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(50.0))),
                    suffixIcon: IconButton(
                      // onPressed: () {
                      //   controller.searchUser(textEditingController.text);
                      // },
                      color: AppColors.GREEN,
                      icon: Icon(Icons.search),
                    ),
                    hintText: 'Search ...',
                  ),
                ),
              ),
              UserList()
            ],
          ),
        );
      },
    );
  }
}
