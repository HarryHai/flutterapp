import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_getx_concept/pages/buys_detail/controller.dart';
import 'package:flutter_getx_concept/pages/sells/controller.dart';
import 'package:flutter_getx_concept/pages/sells_detail/controller.dart';
import 'package:flutter_getx_concept/utils/colors.dart';
import 'package:flutter_getx_concept/widgets/appbar_action.dart';
import 'package:flutter_getx_concept/widgets/custom_appbar.dart';
import 'package:flutter_getx_concept/widgets/product_image.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:get/get.dart';

import 'widgets/sell_details.dart';

class SellsDetail extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<SellsDetailController>(
      init: SellsDetailController(),
      builder: (controller) {
        return Scaffold(
          backgroundColor: AppColors.LIGHT,
          appBar: CustomAppBar(
            "Sells detail",
            leadings: [
              CustomAppBarAction(
                () => Get.back(),
                Feather.arrow_left,
              )
            ],
          ),
          body: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Obx(
                  () => Center(
                    child: CachedNetworkImage(
                      imageUrl: controller.sell.buyerMatchInfo.name,
                      imageBuilder: (context, imageProvider) => Container(
                        width: 60.0,
                        height: 60.0,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: imageProvider, fit: BoxFit.cover),
                        ),
                      ),
                      placeholder: (context, url) =>
                          CircularProgressIndicator(),
                      errorWidget: (context, url, error) => ProductImage(
                        "assets/images/ic_sells.png",
                        width: 60,
                        height: 60,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                SellsDetailsInfo(controller.sell)
              ],
            ),
          ),
        );
      },
    );
  }
}
