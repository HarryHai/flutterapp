import 'package:flutter_getx_concept/models/sells.model.dart';
import 'package:flutter_getx_concept/pages/sells/controller.dart';
import 'package:get/get.dart';
import 'package:get/state_manager.dart';

class SellsDetailController extends GetxController {
  SellsController controller = Get.find();
  RxList<SellsModel> rxList = RxList<SellsModel>([]);
  Rx<SellsModel> _sells = Rx<SellsModel>();

  setBuys(SellsModel value) => _sells.value = value;

  SellsModel get sell {
    return _sells.value;
  }

  SellsDetailController() {
    loadSellsDetail();
  }

  loadSellsDetail() async {
    try {
      rxList.addAll(controller.getListSells());
      rxList.forEach((user) {
        if (user.id == Get.parameters["id"].toString()) {
          setBuys(user);
        }
      });
    } catch (error) {
      print(error.toString());
    }
  }
}
