import 'package:flutter/material.dart';
import 'package:flutter_getx_concept/models/sells.model.dart';
import 'package:flutter_getx_concept/pages/buys_detail/controller.dart';
import 'package:flutter_getx_concept/pages/sells_detail/controller.dart';
import 'package:flutter_getx_concept/utils/colors.dart';
import 'package:get/get.dart';

class SellsDetailsInfo extends StatelessWidget {
  final SellsDetailController controller = Get.find();
  final SellsModel sellsModel;

  SellsDetailsInfo(this.sellsModel);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(25),
      decoration: BoxDecoration(
        color: AppColors.WHITE,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25),
          topRight: Radius.circular(25),
        ),
      ),
      child: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(bottom: 25),
              margin: EdgeInsets.only(bottom: 25),
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    color: AppColors.LIGHT,
                    width: 1,
                    style: BorderStyle.solid,
                  ),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Obx(
                    () => Center(
                      child: Text(
                        sellsModel.buyerMatchInfo.name ?? "",
                        style: TextStyle(
                          fontSize: 32,
                          color: AppColors.DARK,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 15),
                  Obx(
                    () => Center(
                      child: Text(
                        sellsModel.phone ?? "",
                        style: TextStyle(
                          fontSize: 14,
                          color: AppColors.DARK,
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Row(
              children: <Widget>[
                Text(
                  "Phone: ",
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.DARK,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  sellsModel.phone ?? "",
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.LIGHT_GREEN,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Text(
                  "Status: ",
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.DARK,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  sellsModel.status ?? "",
                  style: TextStyle(
                    fontSize: 14,
                    color: AppColors.LIGHT_GREEN,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
